
public function newsletter_admin_menu(){
		global $newsletter_page_settings;

		$newsletter_page_settings = add_menu_page('Newsletter', 'Newsletter', 2, 'ath-newsletter', array($this, 'newsletter_page_actions'), $this->dashicon, 12);
		add_action( 'admin_head-' . $newsletter_page_settings, array($this, 'help_tab') );
}


public function help_tab() {

		if (!class_exists('help_tabs')) {
			$class = 'notice notice-error';
			$message = 'No Class "help_tabs". ' . sprintf(__("Install %s now"), 'Plugin (help_tabs)');
			printf( '<div class="%1$s"><p>%2$s</p></div>', $class, $message );
			return '';
		}

		$screen		= get_current_screen();

		$dir = plugin_dir_path( __FILE__ ) . '/helps';
		$help_tabs = new help_tabs( $screen, $dir );
}
