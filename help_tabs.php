<?php
/*
Plugin Name: ATH Help tabs
Plugin URI:
Description: ATH Help tabs
Version: 1.201702.24
Author: Anh-Tuan Hoang
Author URI:
*/

define( 'ATH_HELP_TAB', plugin_dir_path( __FILE__ ) );
if( is_admin() )
{
	new help_tabs();
}
class help_tabs
{
	public $dir;
	public $screen;

	public function __construct( $screen='', $dir='' )
	{
  
		$plugin = plugin_basename(__FILE__);
		add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
		add_filter("plugin_action_links_$plugin", array( $this, 'settings_link' ) );

    if ( $dir ){
      $dir            = realpath( $dir );
                      chmod($dir, 0777);
  		$this->dir      = $dir;
  		$this->screen   = $screen;
  		$this->display();
    }
	}

	public function add_plugin_page() {

		add_options_page(
            'Help Tabs',
            'Help Tabs',
            'manage_options',
            'ath-help-tabs',
            array( $this, 'create_admin_page' )
        );
	}
	public function settings_link($links) {
	  $settings_link = '<a href="options-general.php?page=ath-help-tabs">'. __("Settings") . '</a>';
	  array_unshift($links, $settings_link);
	  return $links;
	}
	/**
     * Options page callback
     */
    public function create_admin_page()
    {
        ?>
        <div class="wrap">
            <h2><?php echo $GLOBALS['title'] ?></h2>
            <?php
            $file_get_contents = file_get_contents( ATH_HELP_TAB . '/readme.txt');
            $file_content = explode("\n", $file_get_contents);

            foreach( $file_content as $fgc ){
                echo esc_html($fgc) .'<br>';
            }
            ?>
        </div>
        <?php
    }
	public function display(){
		$filename = array();
		if ($handle = opendir( $this->dir )) {

			while ($files[] = readdir($handle));
			sort($files);
			// closedir($dir);

			foreach ($files as $filename){

				$content = $this->readfile( $filename );

				$this->screen->add_help_tab( array(
					'id'		=> substr($filename, 0, strrpos($filename, '.')),
					'title'		=> $content["title"],
					'content'	=> $content["content"]
				) );

			}

			closedir($handle);
		}
	}
	public function readfile( $filename ){

		$content = '';
    if ( strlen($filename) < 4 ){
      return ;
    }

    $dir = realpath( $this->dir );
    chmod($dir, 0777);

    $filePath = realpath($dir. "/" . $filename);
    $file_get_contents = file_get_contents( $filePath );
		$file_content = explode("\n", $file_get_contents);

		foreach( $file_content as $fgc ){
			$content .= $fgc .'<br>';
		}
		return array("title" => $file_content[0], "content" => $content);

	}
}
?>
